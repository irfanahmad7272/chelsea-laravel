<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Article;
use simplehtmldom\HtmlWeb;

Route::get('/', function () {
    
	
	$article = new Article;
	$result = $article->all();

	return $result;

});

Route::get('/yasir', function () {

    $client   = new HtmlWeb();
    $html     = $client->load('https://www.mysupermarket.co.il');

    dd($html->find('.BelowTheFoldHP'));
    //dd($html->find('div.FirstColumn', 0));

});

Route::get('/mirror', function () {

	$client   = new HtmlWeb();
    $html     = $client->load('https://www.mirror.co.uk/all-about/arsenal-fc');

    $i = 1;
    foreach($html->find('.teaser') as $ele){

    	$article_url = $ele->find('figure > a', 0)->getAttribute('href');

    	$article_img_url = explode(',', $ele->find('figure > a img', 0)->getAttribute('data-srcset'))[1];

    	$article_title = trim($ele->find('a.headline', 0)->innertext);

    	//go inside single article to fetch date and short description.
    	$sub_client = new HtmlWeb();
    	$article_html = $sub_client->load($article_url);

    	//echo $article_html->find('article.article-main p.sub-title', 0)->innertext . "<br>";

    	$article_date = "";
    	if($article_html->find('time.date-published', 0)){
    		$article_date = date('Y-m-d H:i:s' , strtotime($article_html->find('time.date-published', 0)->getAttribute('datetime')));
    	}
    	else{

    		$article_date = date('Y-m-d H:i:s');

    	}

    	echo $article_date . "<br>";
    	
    	


    }
});
