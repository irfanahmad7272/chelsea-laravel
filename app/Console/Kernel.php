<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Jobs\SkySportsJob;
use App\Jobs\MirrorJob;
use App\Jobs\DailyMailJob;
use App\Jobs\BBCJob;
use App\Jobs\EveningStandardJob;
use App\Jobs\ChelseaFCJob;
use App\Jobs\TheSunJob;
use App\Jobs\MetroJob;
use App\Jobs\DailyStarJob;
use App\Jobs\ExpressJob;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        //express bot
        foreach(config('bots.express.urls') as $bot):

            $schedule->job(new ExpressJob($bot['url'], $bot['team'], config('bots.express.source'), $bot['league']), 'expressQueue')->everyMinute();

        endforeach;

        //dailystar bot
        foreach(config('bots.dailystar.urls') as $bot):

            $schedule->job(new DailyStarJob($bot['url'], $bot['team'], config('bots.dailystar.source'), $bot['league']), 'dailystarQueue')->everyMinute();

        endforeach;

        //metro bot
        foreach(config('bots.metro.urls') as $bot):

            $schedule->job(new MetroJob($bot['url'], $bot['team'], config('bots.metro.source'), $bot['league']), 'metroQueue')->everyMinute();

        endforeach;

        //thesun bot
        foreach(config('bots.thesun.urls') as $bot):

            $schedule->job(new TheSunJob($bot['url'], $bot['team'], config('bots.thesun.source'), $bot['league']), 'sunQueue')->everyMinute();

        endforeach;

        //chelseafc bot
        foreach(config('bots.chelseafc.urls') as $bot):

            $schedule->job(new ChelseaFCJob($bot['url'], $bot['team'], config('bots.chelseafc.source'), $bot['league']), 'fcQueue')->everyMinute();

        endforeach;

        

        //eve_stand bot
        foreach(config('bots.eve_stand.urls') as $bot):

            $schedule->job(new EveningStandardJob($bot['url'], $bot['team'], config('bots.eve_stand.source'), $bot['league']), 'eveStandQueue')->everyFiveMinutes();

        endforeach;

        

        //skysports bot
        foreach(config('bots.sky_sports.urls') as $bot):

            $schedule->job(new SkySportsJob($bot['url'], $bot['team'], config('bots.sky_sports.source'), $bot['league']), 'skySportsQueue')->everyFiveMinutes();

        endforeach;

        // //mirror bot
        foreach(config('bots.mirror.urls') as $bot):

            $schedule->job(new MirrorJob($bot['url'], $bot['team'], config('bots.mirror.source'), $bot['league']), 'mirrorQueue')->everyFiveMinutes();

        endforeach;

        //dailymail bot
        foreach(config('bots.dailymail.urls') as $bot):

            $schedule->job(new DailyMailJob($bot['url'], $bot['team'], config('bots.dailymail.source'), $bot['league']), 'dailymailQueue')->everyFiveMinutes();

        endforeach;

        //bbc bot
        foreach(config('bots.bbc.urls') as $bot):

            $schedule->job(new BBCJob($bot['url'], $bot['team'], config('bots.bbc.source'), $bot['league']), 'bbcQueue')->everyFiveMinutes();

        endforeach;
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
