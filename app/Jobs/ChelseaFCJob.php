<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use simplehtmldom\HtmlWeb;
use App\Article;
use App\Jobs\Child\ChelseafcChildJob;

class ChelseaFCJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $team_url, $team_name, $source_name, $league_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $team, $source, $league)
    {
        $this->team_url = $url;
        $this->team_name = $team;
        $this->source_name = $source;
        $this->league_name = $league;
    }


    function scrapp_data_from_chelseafc(){

        $client   = new HtmlWeb();
        $html     = $client->load($this->team_url);
        $html = $html->find('.news-sections__list', 0);
        $loop_break_limit = 10;

        $article_base_url = "https://www.chelseafc.com";

        if(!empty($html)){

            $i = 1;
            
            foreach($html->find('cfc-news-article-tile') as $ele){

                


                $article_url = trim($ele->find('a.tile', 0)->getAttribute('href'));

                $article_title = trim($ele->find('.tile__description__heading', 0)->innertext);
                
                $add_mins = $i * 3;
                $article_date = date('Y-m-d H:i:s');
                $newtimestamp = strtotime($article_date .' - '. $add_mins .' minute');
                $article_date = date('Y-m-d H:i:s', $newtimestamp);

                //var_dump($article_date);

                if(!empty($article_url)):

                    $article_url = $article_base_url . $article_url;

                    //creating a new child job.
                    ChelseafcChildJob::dispatch($article_url, $this->team_name, $this->source_name, $this->league_name, $article_title, $article_date); 
                    
                endif;




                //control loop exit.
                $i++;
                if($i >= $loop_break_limit){

                    break;
                }
                

            }

        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->scrapp_data_from_chelseafc();
    }
}
