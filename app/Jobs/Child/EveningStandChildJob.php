<?php

namespace App\Jobs\Child;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use simplehtmldom\HtmlWeb;
use App\Article;

class EveningStandChildJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $team_url, $team_name, $source_name, $league_name, $article_title, $article_img_url;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $team, $source, $league, $article_title, $article_img_url)
    {
        $this->team_url = $url;
        $this->team_name = $team;
        $this->source_name = $source;
        $this->league_name = $league;
        $this->article_title = $article_title;
        $this->article_img_url = $article_img_url;
    }

    function scrap_from_child(){

        $client   = new HtmlWeb();
        $html     = $client->load($this->team_url);

        if(!empty($html)){

            $article_date = trim($html->find('.publish-date .amp-timeago', 0)->getAttribute('datetime'));
            if($article_date):

                $article_date = date('Y-m-d H:i:s', strtotime($article_date));

            endif;

            $article_desc = strip_tags(trim($html->find('.body-content  p', 0)->innertext));


                //inserting into db if does not exists...
                $article = new Article;
                $result = $article->firstOrCreate(

                    [
                        'url' => $this->team_url
                    ],

                    [
                        'title' => $this->article_title,

                        'description' => $article_desc,

                        'img_url' => $this->article_img_url,

                        'tag' => "",

                        'posted_date' => $article_date,

                        'team_name' => $this->team_name,

                        'source_name' => $this->source_name,

                        'league_name' => $this->league_name,
                        
                    ]
                );

        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->scrap_from_child();
    }
}
