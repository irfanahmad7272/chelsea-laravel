<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['description', 'title', 'url', 'img_url', 'tag', 'posted_date', 'team_name', 'source_name', 'league_name'];
}
