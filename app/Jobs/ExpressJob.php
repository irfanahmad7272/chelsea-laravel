<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use simplehtmldom\HtmlWeb;
use App\Article;
use App\Jobs\Child\ExpressChildJob;

class ExpressJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $team_url, $team_name, $source_name, $league_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $team, $source, $league)
    {
        $this->team_url = $url;
        $this->team_name = $team;
        $this->source_name = $source;
        $this->league_name = $league;
    }


    function scrapp_data_from_express(){

        $client   = new HtmlWeb();
        $html     = $client->load($this->team_url);
        $html = $html->find('.last.block.three-stories', 0);
        $loop_break_limit = 7;

        $article_base_url = "https://www.express.co.uk";

        if(!empty($html)){

            $i = 1;
            foreach($html->find('li[data-vr-contentbox]') as $ele){

                


                $article_url = trim($ele->find('a', 0)->getAttribute('href'));

                $article_title = trim($ele->find('a h4', 0)->innertext);

                // $article_img_url = trim($ele->find('picture img', 0)->getAttribute('data-src'));

               

                if(!empty($article_url)):

                    $article_url = $article_base_url . $article_url;
                    //creating a new child job.
                    ExpressChildJob::dispatch($article_url, $this->team_name, $this->source_name, $this->league_name, $article_title); 
                    
                endif;




                //control loop exit.
                $i++;
                if($i >= $loop_break_limit){

                    break;
                }
                

            }

        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->scrapp_data_from_express();
    }
}
