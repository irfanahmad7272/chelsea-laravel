<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Sky Sports Bot
    |--------------------------------------------------------------------------
    |
    | All sky sports teams bot urls.
    |
    */

    'sky_sports' => [

		'source' => 'Sky Sports',

    	'urls' => [

            /* -------------------------------- Premier League Teams --------------------------------  */

    		
            [
                'url' => 'https://www.skysports.com/chelsea-news',
                'team' => 'Chelsea',
                'league' => 'Premier League'
            ],
 		
    	],

    ],

    /*
    |--------------------------------------------------------------------------
    | Mirror Bot
    |--------------------------------------------------------------------------
    |
    | All mirror teams bot urls.
    |
    */

    'mirror' => [

        'source' => 'Mirror',

        'urls' => [

            /* -------------------------------- Premier League Teams --------------------------------  */

            
            [
                'url' => 'https://www.mirror.co.uk/all-about/chelsea-fc',
                'team' => 'Chelsea',
                'league' => 'Premier League'
            ],
            

        ]

    ],

    /*
    |--------------------------------------------------------------------------
    | Daily Mail Bot
    |--------------------------------------------------------------------------
    |
    | All dailymail teams bot urls.
    |
    */

    'dailymail' => [

        'source' => 'Daily Mail',

        'urls' => [

            /* -------------------------------- Premier League Teams --------------------------------  */

            
            [
                'url' => 'https://www.dailymail.co.uk/sport/teampages/chelsea.html',
                'team' => 'Chelsea',
                'league' => 'Premier League'
            ],
            
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | BBC Bot
    |--------------------------------------------------------------------------
    |
    | All bbc teams bot urls.
    |
    */

    'bbc' => [

        'source' => 'BBC',

        'urls' => [

            /* -------------------------------- Premier League Teams --------------------------------  */

            
            [
                'url' => 'https://www.bbc.com/sport/football/teams/chelsea',
                'team' => 'Chelsea',
                'league' => 'Premier League'
            ],
            
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | The Evening Standard Bot
    |--------------------------------------------------------------------------
    |
    | All evening standard teams bot urls.
    |
    */

    'eve_stand' => [

        'source' => 'Evening Standard',

        'urls' => [

            /* -------------------------------- Premier League Teams --------------------------------  */

            
            [
                'url' => 'https://www.standard.co.uk/sport/football/chelsea',
                'team' => 'Chelsea',
                'league' => 'Premier League'
            ],
            
            
            
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | ChelseaFC Bot
    |--------------------------------------------------------------------------
    |
    | All evening standard teams bot urls.
    |
    */

    'chelseafc' => [

        'source' => 'Chelseafc',

        'urls' => [

            /* -------------------------------- Premier League Teams --------------------------------  */

            
            [
                'url' => 'https://www.chelseafc.com/en/news',
                'team' => 'Chelsea',
                'league' => 'Premier League'
            ],
            
            
            
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | The Sun
    |--------------------------------------------------------------------------
    |
    | All evening standard teams bot urls.
    |
    */

    'thesun' => [

        'source' => 'The Sun',

        'urls' => [

            /* -------------------------------- Premier League Teams --------------------------------  */

            
            [
                'url' => 'https://www.thesun.co.uk/sport/football/team/1196660/chelsea/',
                'team' => 'Chelsea',
                'league' => 'Premier League'
            ],
            
            
            
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Metro Bot
    |--------------------------------------------------------------------------
    |
    | All evening standard teams bot urls.
    |
    */

    'metro' => [

        'source' => 'Metro',

        'urls' => [

            /* -------------------------------- Premier League Teams --------------------------------  */

            
            [
                'url' => 'https://metro.co.uk/tag/chelsea-fc/',
                'team' => 'Chelsea',
                'league' => 'Premier League'
            ],
            
            
            
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Express Bot
    |--------------------------------------------------------------------------
    |
    | All evening standard teams bot urls.
    |
    */

    'express' => [

        'source' => 'Express',

        'urls' => [

            /* -------------------------------- Premier League Teams --------------------------------  */

            
            [
                'url' => 'https://www.express.co.uk/football/teams/17/chelsea',
                'team' => 'Chelsea',
                'league' => 'Premier League'
            ],
            
            
            
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | DailyStar Bot
    |--------------------------------------------------------------------------
    |
    | All evening standard teams bot urls.
    |
    */

    'dailystar' => [

        'source' => 'Daily Star',

        'urls' => [

            /* -------------------------------- Premier League Teams --------------------------------  */

            
            [
                'url' => 'https://www.dailystar.co.uk/latest/chelsea-fc',
                'team' => 'Chelsea',
                'league' => 'Premier League'
            ],
            
            
            
        ]
    ],

];