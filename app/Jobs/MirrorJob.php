<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use simplehtmldom\HtmlWeb;
use App\Article;

class MirrorJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $team_url, $team_name, $source_name, $league_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $team, $source, $league)
    {
        $this->team_url = $url;
        $this->team_name = $team;
        $this->source_name = $source;
        $this->league_name = $league;
    }

    public function scrapp_data_from_mirror()
    {

        $client   = new HtmlWeb();
        $html     = $client->load($this->team_url);

        if(!empty($html)){

            foreach($html->find('.teaser') as $ele){

                $article_url = $ele->find('figure > a', 0)->getAttribute('href');

                $article_img_url = explode(',', $ele->find('figure > a img', 0)->getAttribute('data-srcset'))[1];

                $article_title = trim($ele->find('a.headline', 0)->innertext);

                $article_tag = trim($ele->find('a.label', 0)->innertext);

                //go inside single article to fetch date and short description.
                $sub_client = new HtmlWeb();
                $article_html = $sub_client->load($article_url);

                $article_desc = $article_html->find('article.article-main p.sub-title', 0)->innertext;

                $article_date = "";
                if($article_html->find('time.date-published', 0)){
                    $article_date = date('Y-m-d H:i:s' , strtotime($article_html->find('time.date-published', 0)->getAttribute('datetime')));
                }
                else{

                    $article_date = date('Y-m-d H:i:s');

                }

                //dump($article_title);exit;

                //inserting into db if does not exists...
                $article = new Article;
                $result = $article->firstOrCreate(

                    [
                        'url' => $article_url
                    ],

                    [
                        'title' => $article_title,

                        'description' => $article_desc,

                        'img_url' => $article_img_url,

                        'tag' => $article_tag,

                        'posted_date' => $article_date,

                        'team_name' => $this->team_name,

                        'source_name' => $this->source_name,

                        'league_name' => $this->league_name,
                        
                    ]
                );
                

            }

            dump('Job is done under source: '. $this->source_name .' and under league: '. $this->league_name . ' and under team: '. $this->team_name);

        }

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->scrapp_data_from_mirror();
    }
}
