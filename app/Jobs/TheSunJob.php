<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use simplehtmldom\HtmlWeb;
use App\Article;
use App\Jobs\Child\TheSunChildJob;

class TheSunJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $team_url, $team_name, $source_name, $league_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $team, $source, $league)
    {
        $this->team_url = $url;
        $this->team_name = $team;
        $this->source_name = $source;
        $this->league_name = $league;
    }


    function scrapp_data_from_thesun(){

        $client   = new HtmlWeb();
        $html     = $client->load($this->team_url);
        $html = $html->find('.sun-row.teaser', 0);
        $loop_break_limit = 7;

        $article_base_url = "https://www.thesun.co.uk";

        if(!empty($html)){

            $i = 1;
            foreach($html->find('.teaser-item.theme-football') as $ele){

                


                $article_url = trim($ele->find('a.text-anchor-wrap', 0)->getAttribute('href'));

                $article_title = trim($ele->find('.teaser__subdeck', 0)->innertext);

                $article_img_url = trim($ele->find('.delayed-image-load-mobile', 0)->getAttribute('data-src'));

                

                if(!empty($article_url)):

                    //creating a new child job.
                    TheSunChildJob::dispatch($article_url, $this->team_name, $this->source_name, $this->league_name, $article_title, $article_img_url); 
                    
                endif;




                //control loop exit.
                $i++;
                if($i >= $loop_break_limit){

                    break;
                }
                

            }

        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->scrapp_data_from_thesun();
    }
}
