<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use simplehtmldom\HtmlWeb;
use App\Article;

class ChildArticleJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $team_url, $team_name, $source_name, $league_name, $article_title;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $team, $source, $league, $title)
    {
        $this->team_url = $url;
        $this->team_name = $team;
        $this->source_name = $source;
        $this->league_name = $league;
        $this->article_title = $title;
    }

    function scrap_from_child(){

        $client   = new HtmlWeb();
        $html     = $client->load($this->team_url);

        if(!empty($html)){

            $article_date = trim($html->find('.article-timestamp-published time', 0)->getAttribute('datetime'));
            if($article_date):

                $article_date = date('Y-m-d H:i:s', strtotime($article_date));

            endif;

            $article_desc = trim(strip_tags($html->find('div[itemprop=articleBody] p', 0)->innertext));
            
            $article_img_url = trim($html->find('.mol-img .image-wrap img', 0)->getAttribute('data-src'));

                //inserting into db if does not exists...
                $article = new Article;
                $result = $article->firstOrCreate(

                    [
                        'url' => $this->team_url
                    ],

                    [
                        'title' => $this->article_title,

                        'description' => $article_desc,

                        'img_url' => $article_img_url,

                        'tag' => "",

                        'posted_date' => $article_date,

                        'team_name' => $this->team_name,

                        'source_name' => $this->source_name,

                        'league_name' => $this->league_name,
                        
                    ]
                );

        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $this->scrap_from_child();

        dump('Job is done under source: '. $this->source_name .' and under league: '. $this->league_name . ' and under team: '. $this->team_name);
    }
}
