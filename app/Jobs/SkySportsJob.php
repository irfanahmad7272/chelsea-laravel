<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use simplehtmldom\HtmlWeb;
use App\Article;

class SkySportsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $team_url, $team_name, $source_name, $league_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $team, $source, $league)
    {
        $this->team_url = $url;
        $this->team_name = $team;
        $this->source_name = $source;
        $this->league_name = $league;
    }

    function get_formatted_date($datetime){

        $d = explode(' ', $datetime);
        $date = explode('/' , $d[0]);

        //returning date in y-m-d h:i:s format...

        $tmp_date = strtotime($date[2] . '-' . $date[1] . '-' . $date[0] . ' ' . $d[1]);

        return date('Y-m-d H:i:s' , $tmp_date);
    }

    public function scrapp_data_from_skysports()
    {

        $articles_limit = 10;
        $articles       = array();

        $client   = new HtmlWeb();
        $html     = $client->load($this->team_url);

        //dd($html->find('.news-list__item', 0));


        //for ($i = 0; $i <= $articles_limit; $i++) {
        foreach($html->find('.news-list__item') as $ele){

            //Article object...
            $article = new Article;


            $posted_date = $ele->find('.label__timestamp', 0)->innertext;
            if(!empty($posted_date)):

                $posted_date = $this->get_formatted_date($posted_date);
                

            endif;

            //inserting into db if does not exists...
            $result = $article->firstOrCreate(

                [
                    
                    'url' => trim($ele->find('.news-list__headline-link', 0)->getAttribute('href'))
                ],

                [
                    'description' => $ele->find('p.news-list__snippet', 0)->innertext,

                    'title' => trim($ele->find('.news-list__headline-link', 0)->innertext),

                    'img_url' => $ele->find('.news-list__image', 0)->getAttribute('data-src'),

                    'tag' => trim($ele->find('.label__tag', 0)->innertext),

                    'posted_date' => $posted_date,

                    'team_name' => $this->team_name,

                    'source_name' => $this->source_name,

                    'league_name' => $this->league_name,
                    
                ]
            );

            
        }

        dump('Job is done under source: '. $this->source_name .' and under league: '. $this->league_name . ' and under team: '. $this->team_name);


    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->scrapp_data_from_skysports();
        //logger('job is done');
        //dump('job is done');
    }
}
