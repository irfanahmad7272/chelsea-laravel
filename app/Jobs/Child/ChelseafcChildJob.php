<?php

namespace App\Jobs\Child;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use simplehtmldom\HtmlWeb;
use App\Article;

class ChelseafcChildJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $team_url, $team_name, $source_name, $league_name, $article_title, $article_date;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $team, $source, $league, $article_title, $article_date)
    {
        $this->team_url = $url;
        $this->team_name = $team;
        $this->source_name = $source;
        $this->league_name = $league;
        $this->article_title = $article_title;
        $this->article_date = $article_date;
    }


     function scrap_from_child(){

        $client   = new HtmlWeb();
        $html     = $client->load($this->team_url);

        if(!empty($html)){


            $article_desc = strip_tags(trim($html->find('.page-rich-text__content', 0)->getAttribute('innerhtml')));

            $article_img_url = "";
            if($html->find('.standard-article__hero__image', 0)){

                $article_img_url = trim($html->find('.standard-article__hero__image', 0)->getAttribute('data-swapimage'));

                //fetching image with 940px width
                $article_img_url = json_decode($article_img_url);
                $article_img_url = trim($article_img_url[2]->url);

            }
            

            


                //inserting into db if does not exists...
                $article = new Article;
                $result = $article->firstOrCreate(

                    [
                        'url' => $this->team_url
                    ],

                    [
                        'title' => $this->article_title,

                        'description' => $article_desc,

                        'img_url' => $article_img_url,

                        'tag' => "",

                        'posted_date' => $this->article_date,

                        'team_name' => $this->team_name,

                        'source_name' => $this->source_name,

                        'league_name' => $this->league_name,
                        
                    ]
                );

        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->scrap_from_child();
    }
}
