<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use simplehtmldom\HtmlWeb;
use App\Article;

class BBCJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $team_url, $team_name, $source_name, $league_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $team, $source, $league)
    {
        $this->team_url = $url;
        $this->team_name = $team;
        $this->source_name = $source;
        $this->league_name = $league;
    }

    function scrapp_data_from_bbc(){

        $client   = new HtmlWeb();
        $html     = $client->load($this->team_url);
        $html = $html->find('#top-stories .gel-layout__item', 0);
        $loop_break_limit = 5;

        $article_base_url = "https://www.bbc.com";

        if(!empty($html)){

            $i = 1;
            foreach($html->find('article') as $ele){

                $article_img_url = trim($ele->find('.lakeside__media img', 0)->getAttribute('data-src'));
                if(!empty($article_img_url)){

                    $url_arr = explode("{width}{hidpi}", $article_img_url);
                    $article_img_url = trim($url_arr[0]) . "480" . trim($url_arr[1]);
                }
                else{

                    $article_img_url = trim($ele->find('.lakeside__media img', 0)->getAttribute('srcset'));
                    $url_arr = explode(",", $article_img_url);
                    $article_img_url = explode(" ", trim($url_arr[2]));
                    $article_img_url = trim($article_img_url[0]);
                }


                $article_title = trim($ele->find('h3.lakeside__title span.lakeside__title-text', 0)->innertext);

                $article_desc = trim($ele->find('.lakeside__summary', 0)->innertext);


                $article_url = trim($ele->find('h3.lakeside__title a', 0)->getAttribute('href'));
                $article_url = $article_base_url . $article_url;

                $article_date = trim($ele->find('.timestamp time', 0)->getAttribute('data-timestamp'));
                if($article_date){

                    $article_date = date('Y-m-d H:i:s', $article_date);
                }


                if(!empty($article_url)):

                    //inserting into db if does not exists...
                    $article = new Article;
                    $result = $article->firstOrCreate(

                        [
                            'url' => $article_url
                            //'title' => $article_title,
                        ],

                        [
                            'title' => $article_title,

                            'description' => $article_desc,

                            'img_url' => $article_img_url,

                            'tag' => "",

                            'posted_date' => $article_date,

                            'team_name' => $this->team_name,

                            'source_name' => $this->source_name,

                            'league_name' => $this->league_name,
                            
                        ]
                    );
                    
                endif;


                //control loop exit.
                $i++;
                if($i >= $loop_break_limit){

                    break;
                }
                

            }

        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->scrapp_data_from_bbc();

    }
}
