<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use simplehtmldom\HtmlWeb;
use App\Article;
use App\Jobs\ChildArticleJob;

class DailyMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $team_url, $team_name, $source_name, $league_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $team, $source, $league)
    {
        $this->team_url = $url;
        $this->team_name = $team;
        $this->source_name = $source;
        $this->league_name = $league;
    }

    function scrapp_data_from_dailymail(){

        $client   = new HtmlWeb();
        $html     = $client->load($this->team_url);

        $dailymail_base_url = "https://www.dailymail.co.uk";

        if(!empty($html)){


            foreach($html->find('.football-team-news .article') as $ele){

                $article_url = trim($ele->find('h2.linkro-darkred > a', 0)->getAttribute('href'));

                $article_title = trim($ele->find('h2.linkro-darkred > a', 0)->innertext);


                if(!empty($article_url)):

                    $article_url = $dailymail_base_url . $article_url;

                    //creating a new child job.
                    ChildArticleJob::dispatch($article_url, $this->team_name, $this->source_name, $this->league_name, $article_title); 
                    
                endif;
                

            }

        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->scrapp_data_from_dailymail();
    }
}
