<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use simplehtmldom\HtmlWeb;
use App\Article;
use App\Jobs\Child\EveningStandChildJob;

class EveningStandardJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $team_url, $team_name, $source_name, $league_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $team, $source, $league)
    {
        $this->team_url = $url;
        $this->team_name = $team;
        $this->source_name = $source;
        $this->league_name = $league;
    }

    function scrapp_data_from_eve_stand(){

        $client   = new HtmlWeb();
        $html     = $client->load($this->team_url);

        $_base_url = "https://www.standard.co.uk";

        if(!empty($html)){


            foreach($html->find('.four-articles .article') as $ele){

                $article_url = trim($ele->find('.img a', 0)->getAttribute('href'));

                
                $article_title = trim($ele->find('.headline', 0)->innertext);

                $image_url = trim($ele->find('.amp-img', 0)->getAttribute('srcset'));

                if($image_url){

                    $image_url = explode(',', $image_url)[1];
                    $image_url = explode(' ', trim($image_url))[0];
                }

                if(!empty($article_url)):

                    $article_url = $_base_url . $article_url;

                    //creating a new child job.
                    EveningStandChildJob::dispatch($article_url, $this->team_name, $this->source_name, $this->league_name, $article_title, $image_url); 
                    
                endif;
                

            }

        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->scrapp_data_from_eve_stand();
    }
}
