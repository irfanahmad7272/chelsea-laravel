<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->string('title')->unique();
            $table->mediumText('description')->nullable();
            $table->string('url')->unique();
            $table->string('img_url')->nullable();
            $table->string('tag')->nullable();
            $table->dateTime('posted_date')->nullable();
            $table->string('team_name')->nullable();
            $table->string('source_name')->nullable();
            $table->string('league_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
